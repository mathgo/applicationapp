#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
docker run --link pgdb:pg -v $DIR:/var/www/ -p 8080:80 -d mtg42/symfony2-nginx
