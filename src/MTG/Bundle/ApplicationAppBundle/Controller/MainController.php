<?php

namespace MTG\Bundle\ApplicationAppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class MainController extends Controller
{
    /**
     * A lil bit too magic for me, but its an interesting feature
     *
     * @Template
     */
    public function indexAction($name) {}

    public function securedAction()
    {
        return new Response('Login');
    }
}
