<?php

namespace MTG\Bundle\ApplicationAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

class SecurityController extends Controller
{

    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        $auth_error = SecurityContextInterface::AUTHENTICATION_ERROR;
        $error = '';

        // get the login error if there is one
        if ($request->attributes->has($auth_error)) {
            $error = $request->attributes->get($auth_error);
        } elseif (null !== $session && $session->has($auth_error)) {
            $error = $session->get($auth_error);
            $session->remove($auth_error);
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' :
                        $session->get(SecurityContextInterface::LAST_USERNAME);

        return $this->render(
            'MTGApplicationAppBundle:Security:login.html.twig',
            array('last_username' => $lastUsername, 'error' => $error)
        );
    }
}
