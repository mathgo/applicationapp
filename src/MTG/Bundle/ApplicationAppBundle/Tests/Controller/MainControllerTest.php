<?php

namespace MTG\Bundle\ApplicationAppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MainControllerTest extends WebTestCase
{
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testIndex()
    {
        $crawler = $this->client->request('GET', '/');
        $this->assertCount(1, $crawler->filter('html:contains("Hello World")'));

        $crawler = $this->client->request('GET', '/Manfred');
        $this->assertCount(1, $crawler->filter('html:contains("Hello Manfred")'));
        $this->assertCount(0, $crawler->filter('html:contains("Hello World")'));
    }

    public function testSecured()
    {
        $crawler  = $this->client->request('GET', '/secured');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $crawler->filter('html:contains("Login")'));
    }
}
